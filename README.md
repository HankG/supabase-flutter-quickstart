# supabase-flutter-quickstart

This is the finished source code for the Supabase Flutter Quickstart tutorial I wrote using the new Supabase Flutter SDK 1.x preview: https://nequalsonelifestyle.com/2022/08/10/supabase-flutter-quickstart-new-api/


To use simply check the code out:

```bash
git clone https://gitlab.com/HankG/supabase-flutter-quickstart.git
```

To run in web mode:

```bash
cd supabase-flutter-quickstart
flutter run -d chrome
```

## License
This code is Apache 2.0 licensed.
