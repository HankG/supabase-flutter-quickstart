import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/account_page.dart';
import 'package:supabase_quickstart/constants.dart';
import 'package:supabase_quickstart/login_page.dart';
import 'package:supabase_quickstart/splash_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Supabase.initialize(
    url: '<YourURL>',
    anonKey:
        '<YourAnonKey>',
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Supabase.instance.client.auth.onAuthStateChange((event, session) {
      switch (event) {
        case AuthChangeEvent.signedIn:
          Navigator.of(globalAppNavigatorKey.currentContext!)
              .pushNamedAndRemoveUntil('/account', (route) => false);
          break;
        case AuthChangeEvent.signedOut:
          Navigator.of(globalAppNavigatorKey.currentContext!)
              .pushNamedAndRemoveUntil('/login', (route) => false);
          break;
        default:
          break;
      }
    });

    return MaterialApp(
        navigatorKey: globalAppNavigatorKey,
        title: 'Supabase Flutter Quickstart',
        theme: ThemeData.dark().copyWith(
            primaryColor: Colors.green,
            elevatedButtonTheme: ElevatedButtonThemeData(
                style: ElevatedButton.styleFrom(
              onPrimary: Colors.white,
              primary: Colors.green,
            ))),
        initialRoute: supabase.auth.currentUser != null ? '/account' : '/login',
        routes: <String, WidgetBuilder>{
          '/': (_) => const SplashPage(),
          '/login': (_) => const LoginPage(),
          '/account': (_) => const AccountPage(),
        });
  }
}
