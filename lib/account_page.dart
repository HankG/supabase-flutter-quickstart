import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:supabase_quickstart/constants.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  final _usernameController = TextEditingController();
  final _websiteController = TextEditingController();
  var _loading = false;

  @override
  void initState() {
    super.initState();
    final user = supabase.auth.currentUser;
    if (user == null) {
      Navigator.of(globalAppNavigatorKey.currentContext!)
          .pushNamedAndRemoveUntil('/login', (route) => false);
    }
    _getProfile(user!.id);
  }

  Future<void> _getProfile(String userId) async {
    setState(() {
      _loading = true;
    });

    try {
      final data =
          await supabase.from('profiles').select().eq('id', userId).single();
      if (data != null) {
        _usernameController.text = (data['username'] ?? '') as String;
        _websiteController.text = (data['website'] ?? '') as String;
      }
    } on PostgrestException catch (error) {
      // 406 error is okay, means that the profile data hasn't been set yet
      if (error.code != "406") {
        context.showErrorSnackBar(message: error.message);
      }
    }

    setState(() {
      _loading = false;
    });
  }

  Future<void> _updateProfile() async {
    setState(() {
      _loading = true;
    });

    final userName = _usernameController.text;
    final website = _websiteController.text;
    final user = supabase.auth.currentUser;
    final updates = {
      'id': user!.id,
      'username': userName,
      'website': website,
      'updated_at': DateTime.now().toIso8601String(),
    };

    try {
      await supabase.from('profiles').upsert(updates);
      context.showSnackBar(message: 'Successfully updated profile!');
    } catch (error) {
      context.showErrorSnackBar(message: error.toString());
    }

    setState(() {
      _loading = false;
    });
  }

  Future<void> _signOut() async {
    try {
      await supabase.auth.signOut();
    } catch (error) {
      context.showErrorSnackBar(message: error.toString());
    }
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _websiteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Profile')),
        body: ListView(
          padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 12),
          children: [
            Text('User email: ${supabase.auth.currentUser?.email}'),
            TextFormField(
              controller: _usernameController,
              decoration: const InputDecoration(labelText: 'User Name'),
            ),
            const SizedBox(height: 18),
            TextFormField(
              controller: _websiteController,
              decoration: const InputDecoration(labelText: 'Website'),
            ),
            const SizedBox(height: 18),
            ElevatedButton(
              onPressed: _updateProfile,
              child: Text(_loading ? 'Saving...' : 'Update'),
            ),
            const SizedBox(height: 18),
            ElevatedButton(
              onPressed: _signOut,
              child: const Text('Sign Out'),
            ),
          ],
        ));
  }
}
